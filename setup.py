from setuptools import setup, find_packages

setup(
    name="calculator",
    version="0.1",
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    install_requires=["Click", "tabulate"],
    entry_points="""
        [console_scripts]
        calculator=nginx_logs_calculator.main:prepare_args_for_logs_parser
    """,
)
