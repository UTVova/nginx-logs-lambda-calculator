import re
from collections import defaultdict
from datetime import datetime
from decimal import Decimal
from typing import Optional, Iterable, List, Tuple, Dict
from urllib.parse import urlparse


class Parser:
    regex_text = (
        r"(\d{1,3}\.?)+[\s-]+"
        r"\[(?P<datetime>\d\d\/\w+\/\d+:[:\d]+\s\+\d{4})\]\s"
        r"\"(?P<method>(GET|POST|PUT|PATCH|DELETE)+)\s(?P<path>%s.+)\sHTTP\/\d.\d\"\s"
        r"(?P<status>(2\d{2}|4\d{2}))\s(?P<size>\d+)\s(?P<duration>[\d.]+)?\s\".+\"\s\".+\""
    )
    uuid_regex = re.compile(
        r"[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"
    )

    def __init__(
        self,
        average_time: Optional[Decimal] = None,
        api_prefixes: Optional[List] = None,
        api_prefixes_to_ignore: Optional[List[str]] = None,
        regex: Optional[str] = None,
        **_,
    ):
        self.time_present = average_time is not None
        self.average_time = average_time
        self.api_prefixes = api_prefixes if api_prefixes else ["/"]
        self.api_prefixes_to_ignore = (
            api_prefixes_to_ignore if api_prefixes_to_ignore else []
        )
        self.regex = re.compile(
            self.regex_text % f"({'|'.join(api_prefixes)})" if not regex else regex
        )

    def get_datetime_and_duration(self, match) -> Tuple[datetime, str]:
        try:
            dt = datetime.strptime(match["datetime"], "%d/%b/%Y:%H:%M:%S %z")
        except KeyError:
            raise KeyError("named group 'duration' is not found in regex")
        try:
            duration = match["duration"]
        except KeyError:
            if self.time_present:
                raise KeyError("Duration is neither provided nor found in logs")
            duration = self.average_time
        return dt, duration

    @classmethod
    def unify_path_from_match(cls, match) -> str:
        path: str = urlparse(match["path"]).path
        parts = path.split("/")
        return "/".join(
            "{id}"
            if path_part.isdecimal()
            else "{uuid}"
            if cls.uuid_regex.match(path_part)
            else path_part
            for i, path_part in enumerate(parts, start=1)
        )

    def check_path_with_prefixes(self, path: str) -> bool:
        return any(path.startswith(prefix) for prefix in self.api_prefixes) and not any(
            path.startswith(pref) for pref in self.api_prefixes_to_ignore
        )

    def parse(self, logs: Iterable[str]) -> Tuple[Decimal, Dict[str, List[str]]]:
        logs = filter(lambda m: m is not None, map(self.regex.match, logs))
        total_counter = 1
        url_stats: Dict[str, List[str]] = defaultdict(list)
        first_dt: Optional[datetime] = None
        last_dt: Optional[datetime] = None

        for match in logs:
            dt, request_duration = self.get_datetime_and_duration(match)

            path = self.unify_path_from_match(match)
            if not self.check_path_with_prefixes(path):
                continue
            url_stats[path].append(request_duration)

            if first_dt is None:
                first_dt = dt
            last_dt = dt
            total_counter += 1

        if total_counter < 2:
            raise ValueError("No valid log records were recognised")

        hours_recorded = Decimal((last_dt - first_dt).total_seconds()) / 3600
        return hours_recorded, url_stats
