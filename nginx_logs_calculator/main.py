from decimal import Decimal
from typing import Iterable

import click
from tabulate import tabulate

from .logs_parser import Parser
from .prices_handler import EC2Prices, LambdaCalculator, HOURS_IN_PERIOD


def output_instances_to_choose(instances):
    # formatting shit
    click.echo("Please choose one of the following instances:")
    click.echo(
        tabulate(
            [
                (i, name_price[0], name_price[1])
                for i, name_price in enumerate(instances, start=1)
            ],
            headers=("№", "type", "price in USD"),
        )
    )


@click.command(
    help="Calculate AWS Lambda cost basing on nginx logs and compare to Amazon EC2 cost."
)
@click.argument("logs-file", type=click.File("r", lazy=True))
@click.option(
    "--regex",
    help="regular expression to use on log records. "
    "named groups with names 'datetime', 'duration' and 'path' are required.",
)
@click.option(
    "-prefixes",
    "--api-prefixes",
    default=["/"],
    help="prefixes of url's 'path' part to be recognized as request to API",
    multiple=True,
)
@click.option(
    "-ignore",
    "--api-prefixes-to-ignore",
    default=[],
    help="prefix of url's 'path' part to be ignored as requests to API",
    multiple=True,
)
@click.option(
    "-avg",
    "--average-time",
    type=Decimal,
    help="average time of request time per function in seconds. "
    "use it if you didn't enable 'request_time' in logging. used only if no time found in logs",
)
@click.option("--lambda-ram", type=int, help="average value of RAM per lambda function")
@click.option(
    "-n", "--servers-number", type=int, default=1, help="number of EC2 servers to use"
)
@click.option("-cpus", type=int, default=1, help="number of vCPUs for EC2 instance")
@click.option("-ram", type=Decimal, default=2, help="memory in GiB for EC2 instance")
@click.option("-instance", "--instance-type", type=str, help="instance type, if known")
@click.option("-rate", type=Decimal, help="hour rate for using server in USD, if known")
@click.option("--include-free-usage-tier", is_flag=True)
@click.option(
    "--all-stats", is_flag=True, help="output all urls' paths with percentages of usage"
)
@click.option(
    "--no-stats", is_flag=True, help="if no output with stats of usage is needed"
)
def prepare_args_for_logs_parser(
    logs_file,
    rate: Decimal,
    cpus: int,
    ram: int,
    instance_type: str,
    servers_number: int,
    all_stats: bool,
    no_stats: bool,
    **kwargs,
):
    if logs_file.isatty():
        raise click.ClickException(
            "use '--logs-file' option to specify logs file path or pass a file as stdin"
        )
    else:
        click.Abort()
    api_prefixes: Iterable[str] = kwargs.get("api_prefixes")
    if list(api_prefixes) != ["/"] and any(
        any(to_ignore.startswith(prefix) for prefix in api_prefixes)
        for to_ignore in kwargs.get("api_prefixes_to_ignore", [])
    ):
        raise click.ClickException(
            "api prefixes to ignore cannot start with api prefixes to scan"
        )

    if not rate:
        ec2_prices = EC2Prices()
        if instance_type:
            try:
                rate = ec2_prices.find_instance_by_name(instance_type)
            except ValueError:
                raise click.ClickException(
                    f"No EC2 instances with name {instance_type} found"
                )
        elif cpus and ram:
            instances_with_prices = sorted(
                ec2_prices.find_instances_by_vcpu_and_ram(cpus, ram), key=lambda x: x[1]
            )
            if len(instances_with_prices) == 0:
                raise click.ClickException(
                    f"No instances with RAM={ram} and vCPUs={cpus} are found"
                )
            click.echo(f"RAM: {ram}, number of vCPUs: {cpus}")

            output_instances_to_choose(instances_with_prices)

            instances_types_set = set(i[0] for i in instances_with_prices)
            range_to_choose = set(map(str, range(1, len(instances_with_prices) + 1)))
            while True:
                choice = click.prompt("Choose number of instance")
                if choice not in range_to_choose.union(instances_types_set):
                    click.echo(
                        "Chosen number does not present in list.\nChoose one of the provided instances"
                    )
                    continue
                elif choice in instances_types_set:
                    rate = next(
                        filter(lambda x: x[0] == choice, instances_with_prices)
                    )[1]
                elif choice in range_to_choose:
                    rate = instances_with_prices[int(choice) - 1][1]
                break

    ec2_cost = rate * HOURS_IN_PERIOD * servers_number

    click.echo(f"Path prefixes to scan: {', '.join(api_prefixes)}")
    try:
        with click.progressbar(iter(logs_file), label="Processing logs") as logs:
            hours_parsed, function_times_mapping = Parser(**kwargs).parse(logs)
    except BaseException as e:
        raise click.ClickException(e)
    else:
        lambda_ram = kwargs.pop("lambda_ram", None)
        lambda_cost, percent_for_functions, used_lambda_ram = LambdaCalculator(
            **kwargs
        ).calculate_month_lambda_price(
            hours_parsed,
            function_times_mapping,
            lambda_ram if lambda_ram else Decimal(ram * 1024 / 4),
        )

        functions_stats_output = (
            (func, f"{percent*100:.2f}", f"{lambda_cost*percent:.2f}")
            for func, percent in sorted(
                percent_for_functions.items(), key=lambda x: x[1], reverse=True
            )[: 10 if not (all_stats or no_stats) else None]
        )

        click.echo(f"Used average size of lambda: {used_lambda_ram}")
        click.echo("-" * 60)
        click.secho(
            f"EC2 cost: ${ec2_cost:.2f}",
            fg=("green" if ec2_cost < lambda_cost else "red"),
        )
        click.secho(
            f"Lambda cost: ${lambda_cost:.2f}",
            fg=("green" if lambda_cost < ec2_cost else "red"),
        )
        default_headers = ("path", "% of cost")

        if not no_stats:
            click.echo("Usage statistics by URL paths:")
            if lambda_cost == 0:
                click.echo(
                    tabulate(
                        (
                            (path, percent)
                            for path, percent, _ in functions_stats_output
                        ),
                        headers=default_headers,
                    )
                )
            else:
                click.echo(
                    tabulate(functions_stats_output, headers=(*default_headers, "$"))
                )
