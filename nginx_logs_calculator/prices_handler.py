import csv
import os
from decimal import Decimal
from typing import List, Tuple, Dict, Union

from nginx_logs_calculator import ROOT_DIR

quantizer = Decimal("1.0")

# Lambda
LAMBDA_PRICE_PER_1M_REQUESTS = Decimal("0.2")
LAMBDA_PRICE_FOR_GB_SECOND = Decimal("0.0000166667")
LAMBDA_FREE_NUMBER_OF_REQUESTS = Decimal("1000000")
LAMBDA_FREE_COMPUTATIONS = Decimal("400000")

HOURS_IN_PERIOD = 720  # 1 month


class EC2Prices:
    def __init__(self):
        with open(os.path.join(ROOT_DIR, "ec2-prices.csv"), "r", newline="") as file:
            self.prices = list(csv.DictReader(file))

    def find_instances_by_vcpu_and_ram(
        self, vcpu: int, ram: int
    ) -> List[Tuple[str, Decimal]]:
        return list(
            (row["type"], Decimal(row["$"]))
            for row in self.prices
            if int(row["vCPU"]) == vcpu and Decimal(row["RAM"]) == ram
        )

    def find_instance_by_name(self, name: str) -> Decimal:
        try:
            return Decimal(
                next(filter(lambda row: row["type"] == name, self.prices))["$"]
            )
        except StopIteration:  # filtering gave empty sequence
            raise ValueError


def _ceil(n):
    return Decimal(n).quantize(quantizer)


def _round_up_to_closest(n, base=64):
    """Used to choose size of RAM for lambda function"""
    x, y = divmod(n, base)
    return (x if y == 0 else x + 1) * base


class LambdaCalculator:
    def __init__(self, include_free_usage_tier: bool = True, **_):
        self.include_free_usage_tier = include_free_usage_tier

    @staticmethod
    def apply_free_usage_tier(compute_usage, n_requests):
        return (
            Decimal(0)
            if LAMBDA_FREE_COMPUTATIONS > compute_usage
            else compute_usage - LAMBDA_FREE_COMPUTATIONS,
            Decimal(0)
            if LAMBDA_FREE_NUMBER_OF_REQUESTS > n_requests
            else n_requests - LAMBDA_FREE_NUMBER_OF_REQUESTS,
        )

    def calculate_month_lambda_price(
        self,
        hours_parsed: Decimal,
        durations_in_sec: Dict[str, List[str]],
        lambda_ram: Union[int, float, Decimal],
    ):
        if lambda_ram < 128:
            lambda_ram = 128
        elif lambda_ram > 3008:
            lambda_ram = 3008
        else:
            lambda_ram = _round_up_to_closest(lambda_ram, 64)

        total = {"compute_usage": Decimal(0), "requests": Decimal(0)}
        func_prices: Dict[str, Decimal] = {}
        # calculate compute usage and number of requests in total
        # also calculate cost of requests for every unified path
        for func, durations in durations_in_sec.items():
            func_n_requests = len(durations)
            func_compute_usage = sum(
                path_duration * lambda_ram / 1024
                for path_duration in map(_ceil, durations)
            )
            func_requests_price = (
                Decimal(func_n_requests) * LAMBDA_PRICE_PER_1M_REQUESTS / 1_000_000
            )
            func_price = (
                func_compute_usage * LAMBDA_PRICE_FOR_GB_SECOND + func_requests_price
            )

            # store compute usage, requests number and price for function usage separately
            total["compute_usage"] += func_compute_usage
            total["requests"] += func_n_requests
            func_prices[func] = func_price

        # calculate price for usage of all functions
        total_price_before = (
            total["compute_usage"] * LAMBDA_PRICE_FOR_GB_SECOND
            + total["requests"] * LAMBDA_PRICE_PER_1M_REQUESTS / 1_000_000
        )
        # and calculate contribution of each function in percents
        func_percents = {k: v / total_price_before for k, v in func_prices.items()}

        # multiply price by coefficient to get price for month usage
        coefficient_to_get_month = HOURS_IN_PERIOD / hours_parsed
        total = {k: v * coefficient_to_get_month for k, v in total.items()}

        if self.include_free_usage_tier:
            total["compute_usage"], total["requests"] = self.apply_free_usage_tier(
                total["compute_usage"], total["requests"]
            )

        final_price = (
            total["compute_usage"] * LAMBDA_PRICE_FOR_GB_SECOND
            + total["requests"] / 1_000_000 * LAMBDA_PRICE_PER_1M_REQUESTS
        )
        return final_price, func_percents, lambda_ram
